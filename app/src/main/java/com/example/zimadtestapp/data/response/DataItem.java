package com.example.zimadtestapp.data.response;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("title")
	private String title;

	@SerializedName("url")
	private String url;

	private int index;

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"title = '" + title + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}
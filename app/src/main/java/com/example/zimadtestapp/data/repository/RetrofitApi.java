package com.example.zimadtestapp.data.repository;

import android.app.Application;

import com.example.zimadtestapp.BuildConfig;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Dispatcher;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApi {

    private static RetrofitApi api = null;
    private Application application;
    private Retrofit retrofit;
    private ZiMadApiInterface ziMadApiImpl;
    Dispatcher dispatcher;

    static public RetrofitApi getInstance(Application application){
        if (api == null) return new RetrofitApi(application);
        else return api;
    }

    private RetrofitApi(Application application) {
        this.application = application;
        Cache cache = provideHttpCache(application);
        Gson gson = provideGson();
        OkHttpClient client = provideOkHttpClient(cache);
        retrofit = provideRetrofit(gson, client);
        ziMadApiImpl = provideZiMadApiImplementation(retrofit);
    }

    private static final long CONNECTION_TIMEOUT_SEC = 30L;
    private static final long CACHE_SIZE = 10_000_000L;
    private static final String CACHE_FILE_NAME = "http_cache";

    private Cache provideHttpCache(Application application)  {
        long cacheSize = 10L * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    private Gson provideGson()  {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    private OkHttpClient provideOkHttpClient(Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.cache(cache);

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(httpLoggingInterceptor);
        }

        builder.connectTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS);
        builder.readTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS);
        builder.writeTimeout(CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS);
        builder.retryOnConnectionFailure(true);

        dispatcher = new Dispatcher();
        builder.dispatcher(dispatcher);

        builder.addInterceptor( chain -> {
            Request request = chain.request();

            Request.Builder change = request.newBuilder();
            change.header("Content-Type", "application/json");

            return chain.proceed(change.build());
        });

        return builder.build();
    }

    private String bodyToString(RequestBody request) {
        try {
            Buffer buffer = new Buffer();
            if (request != null)
                request.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (IOException e) {
            return "did not work";
        }
    }

    private Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private ZiMadApiInterface provideZiMadApiImplementation(Retrofit retrofit) {
        return retrofit.create(ZiMadApiInterface.class);
    }

    public ZiMadApiInterface getApi(){
        return ziMadApiImpl;
    }
}

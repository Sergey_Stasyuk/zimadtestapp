package com.example.zimadtestapp.data.repository;

import android.app.Application;

import com.example.zimadtestapp.data.response.TabResponse;

import okhttp3.Dispatcher;
import retrofit2.Call;

public class ZiMadApi {

    static ZiMadApi api = null;
    private RetrofitApi retrofit;

    public static ZiMadApi getInstance(Application app){
        if (api == null) return new ZiMadApi(app);
        else return api;
    }

    private ZiMadApi(Application app) {
        this.retrofit = RetrofitApi.getInstance(app);
    }

    public Dispatcher getDispatcher(){
        return retrofit.dispatcher;
    }

    public CancellableCallback getFirstTabData(ResponseCallbackInterface<TabResponse> callback){
        return wrapCallback(retrofit.getApi().getTabsData("cat"), callback);
    }

    public CancellableCallback getSecondTabData(ResponseCallbackInterface<TabResponse> callback){
        return wrapCallback(retrofit.getApi().getTabsData("dog"), callback);
    }

    private<T> CancellableCallback wrapCallback(Call<T> call, ResponseCallbackInterface<T> callback){
        CancellableCallback cancellableCallback = new CancellableCallback<>(callback);
        call.enqueue(cancellableCallback);
        return cancellableCallback;
    }
}

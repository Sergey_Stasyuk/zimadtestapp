package com.example.zimadtestapp.data.repository;

import com.example.zimadtestapp.data.response.TabResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ZiMadApiInterface {

    @GET("api.php/")
    Call<TabResponse> getTabsData(@Query(value="query") String query);

}

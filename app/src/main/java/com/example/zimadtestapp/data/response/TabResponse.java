package com.example.zimadtestapp.data.response;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class TabResponse {

	@SerializedName("data")
	private ArrayList<DataItem> data;

	@SerializedName("message")
	private String message;

	public void setData(ArrayList<DataItem> data){
		this.data = data;
	}

	public ArrayList<DataItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"TabResponse{" +
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}
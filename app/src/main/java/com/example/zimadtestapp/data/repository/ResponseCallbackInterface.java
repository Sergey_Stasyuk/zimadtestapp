package com.example.zimadtestapp.data.repository;

public interface ResponseCallbackInterface<T> {
    void onSuccess(T result);
    void onFailure(String errorMessage, int errorCode);

}

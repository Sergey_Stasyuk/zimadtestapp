package com.example.zimadtestapp.data.repository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import androidx.annotation.NonNull;

import java.io.IOException;

public class CancellableCallback<T> implements Callback<T> {

    static final int ERROR_UNEXPECTED_CODE = 666;
    static final int ERROR_CONVERTING_DATA_CODE = 601;
    static final int ERROR_INTERNET_CONNECTION_CODE = 600;
    static final int ERROR_NOT_FOUND_CODE = 404;
    static final int ERROR_UNAUTHORIZED = 401;

    private ResponseCallbackInterface<T> callback;
    private boolean canceled;

    CancellableCallback(ResponseCallbackInterface<T> callback) {
        this.callback = callback;
        canceled = false;
    }

    public void cancel() {
        canceled = true;
        callback = null;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (!canceled) {
            try{
                callback.onSuccess(response.body());
            }catch (Exception e){
                callback.onFailure("Непредвиденная ошибка", ERROR_CONVERTING_DATA_CODE);
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (!canceled) {
            try {
                String errorMessage;
                int errorCode;
                if (t instanceof IOException) {
                    errorMessage = "Проверьте интернет соединение";
                    errorCode = ERROR_INTERNET_CONNECTION_CODE;
                } else if (t instanceof HttpException) {
                    switch (((HttpException) t).code()) {
                        case ERROR_UNAUTHORIZED: {
                            errorMessage = "Пользователь не авторизован";
                            errorCode = ERROR_UNAUTHORIZED;
                            break;
                        }
                        case ERROR_NOT_FOUND_CODE: {
                            errorMessage = "Страница не найдена";
                            errorCode = ERROR_NOT_FOUND_CODE;
                            break;
                        }
                        default: {
                            errorMessage = "Непредвиденная ошибка";
                            errorCode = ERROR_UNEXPECTED_CODE;
                        }
                    }
                } else if (t instanceof IllegalStateException) {
                    errorMessage = "Ошибка преобразования данных";
                    errorCode = ERROR_CONVERTING_DATA_CODE;
                } else {
                    errorMessage = "Непредвиденная ошибка";
                    errorCode = ERROR_UNEXPECTED_CODE;
                }
                callback.onFailure(errorMessage, errorCode);
            }catch (Exception e){}
        }
    }
}
package com.example.zimadtestapp.mvvm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.zimadtestapp.ui.activities.main.MainActivityViewModel;
import com.example.zimadtestapp.ui.fragments.tab.TabFragmentViewModel;
import com.example.zimadtestapp.ui.fragments.tab_detail.TabDetailFragmentViewModel;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private Application application;

    public ViewModelFactory(Application application) {
        super();
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == MainActivityViewModel.class) {
            return (T) new MainActivityViewModel(application);
        } else if (modelClass == TabFragmentViewModel.class) {
            return (T) new TabFragmentViewModel(application);
        } else if (modelClass == TabDetailFragmentViewModel.class) {
            return (T) new TabDetailFragmentViewModel(application);
        }
        return null;
    }
}

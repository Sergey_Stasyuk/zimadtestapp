package com.example.zimadtestapp.mvvm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.zimadtestapp.ui.App;
import com.example.zimadtestapp.data.repository.ZiMadApi;

public abstract class BaseViewModel extends AndroidViewModel {

    protected ZiMadApi api;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        api = ((App)application).getApi();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        api.getDispatcher().cancelAll();
    }

    public class ResponseWrapper<T> {
        public boolean isSuccess;
        public T result;

        public ResponseWrapper(boolean isSuccess, T result) {
            this.isSuccess = isSuccess;
            this.result = result;
        }
    }
}

package com.example.zimadtestapp.ui.fragments;

public interface FragmentTagInterface {
    String getFragmentTag();
}

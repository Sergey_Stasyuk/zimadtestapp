package com.example.zimadtestapp.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.zimadtestapp.R;
import com.example.zimadtestapp.data.response.DataItem;
import com.example.zimadtestapp.utils.AppUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TabListAdapter extends BaseAdapter<DataItem, TabListAdapter.ViewHolder> {

    private OnClickListener listener;

    public TabListAdapter(ArrayList<DataItem> list, OnClickListener listener) {
        super(list);
        this.listener = listener;
    }

    @Override
    ViewHolder createViewHolder(View view, int layoutId) {
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.tab_list_item;
    }

    class ViewHolder extends BaseAdapter.BaseViewHolder {

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(int pos) {
            ((TextView)itemView.findViewById(R.id.index_text_view)).setText(String.valueOf(pos));
            ((TextView)itemView.findViewById(R.id.title_text_view)).setText(list.get(pos).getTitle());
            setImage(list.get(pos).getUrl());

            itemView.setOnClickListener(v -> {
                DataItem item = list.get(pos);
                item.setIndex(pos);
                ImageView image = itemView.findViewById(R.id.image_view);
                listener.onClick(list.get(pos));
            });
        }

        private void setImage(String url){
            ImageView image = itemView.findViewById(R.id.image_view);
            View progress = itemView.findViewById(R.id.loading_spinner);
            AppUtils.setImage(image, progress, url);
        }
    }

    public interface OnClickListener {
        void onClick(DataItem item);
    }
}

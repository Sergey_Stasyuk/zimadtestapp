package com.example.zimadtestapp.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.zimadtestapp.R;
import com.example.zimadtestapp.mvvm.BaseViewModel;
import com.example.zimadtestapp.ui.App;
import com.example.zimadtestapp.ui.FragmentManagerInterface;
import com.example.zimadtestapp.ui.TabVisibilityListener;
import com.example.zimadtestapp.ui.activities.BaseActivity;

public abstract class BaseFragment<V extends BaseViewModel> extends Fragment implements FragmentTagInterface {

    protected V viewModel;
    private ViewModelProvider.Factory viewModelFactory;

    protected Context baseContext;
    protected BaseActivity<?> baseActivity;

    protected FragmentManagerInterface fragmentManager = FragmentManagerInterface.empty;
    protected TabVisibilityListener tabVisibilityListener = TabVisibilityListener.empty;

    protected View rootView = null;

    @LayoutRes
    protected abstract int layout();
    protected abstract void initialization(View view, boolean isFirstInit);
    protected abstract V provideViewModel(ViewModelProvider.Factory viewModelFactory);
    protected void onRestoreInstanceState(Bundle savedInstanceState){};

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            if (context instanceof BaseActivity) {
                baseActivity = (BaseActivity)context;
                baseContext = baseActivity.getBaseContext();
            }
            if (context instanceof FragmentManagerInterface){
                fragmentManager = (FragmentManagerInterface)context;
            }
            if (context instanceof TabVisibilityListener){
                tabVisibilityListener = (TabVisibilityListener)context;
            }
        }
        catch(Exception e){}
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onRestoreInstanceState(savedInstanceState);
        viewModelFactory = App.get(baseActivity).getViewModelFactory();
        viewModel = provideViewModel(viewModelFactory);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (layout() != 0)
                ? addProgressView(inflater.inflate(layout(), container, false))
                : super.onCreateView(inflater, container, savedInstanceState);
        return rootView == null ? view : rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        boolean isFirstInit = rootView == null;
        rootView = view;
        initialization(view, isFirstInit);
        super.onViewCreated(view, savedInstanceState);
    }

    private View addProgressView(View rootView) {
        FrameLayout rootLayout = new FrameLayout(baseContext);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        rootLayout.setLayoutParams(layoutParams);
        rootLayout.addView(rootView);
        View progressLayout = baseActivity.getLayoutInflater().inflate(R.layout.dialog_progress, null);
        rootLayout.addView(progressLayout);
        return rootLayout;
    }

    protected void showProgress(boolean show){
        View progress = rootView.findViewById(R.id.progress_layout);
        if (progress != null){
            progress.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    protected V injectViewModel(Class<V> typeParameterClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(typeParameterClass);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

package com.example.zimadtestapp.ui.activities.main;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.zimadtestapp.mvvm.BaseViewModel;
import com.example.zimadtestapp.data.repository.ResponseCallbackInterface;
import com.example.zimadtestapp.data.response.TabResponse;

public class MainActivityViewModel extends BaseViewModel {

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
    }

}

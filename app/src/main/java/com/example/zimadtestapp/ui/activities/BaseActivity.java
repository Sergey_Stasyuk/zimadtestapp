package com.example.zimadtestapp.ui.activities;

import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.zimadtestapp.R;
import com.example.zimadtestapp.mvvm.BaseViewModel;
import com.example.zimadtestapp.ui.App;
import com.example.zimadtestapp.ui.FragmentManagerInterface;
import com.example.zimadtestapp.ui.fragments.BaseFragment;

public abstract class BaseActivity<V extends BaseViewModel> extends AppCompatActivity
        implements FragmentManagerInterface {

    protected V viewModel;
    protected ViewModelProvider.Factory viewModelFactory;

    @LayoutRes
    protected abstract int layout();
    protected abstract void initialization(Bundle savedInstanceState);
    protected abstract V provideViewModel(ViewModelProvider.Factory viewModelFactory);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModelFactory = App.get(this).getViewModelFactory();
        viewModel = provideViewModel(viewModelFactory);

        if (layout() != 0) {
            setContentView(layout());
            initialization(savedInstanceState);
        }
    }

    protected V  injectViewModel(Class<V> typeParameterClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(typeParameterClass);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void attachFragment(BaseFragment newFragment, BaseFragment currentFragment) {
        try {
            if (!newFragment.isAdded()) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, newFragment, newFragment.getFragmentTag())
                        .attach(newFragment)
                        .detach(currentFragment)
                        .commit();
            }
        }
        catch(Exception e){}
    }

    @Override
    public void replaceFragment(BaseFragment newFragment) {
        try {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, newFragment, newFragment.getFragmentTag())
                    .commit();
        }
        catch(Exception e){}
    }

    @Override
    public void addFragment(BaseFragment newFragment) {
        try {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, newFragment, newFragment.getFragmentTag())
                    .addToBackStack(null)
                    .commit();
        }
        catch(Exception e){}
    }

    @Override
    @Nullable public BaseFragment getFragmentByTag(String tag){
        FragmentManager manager = getSupportFragmentManager();
        return (BaseFragment) manager.findFragmentByTag(tag);
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
        }catch(Exception e){}
    }

}

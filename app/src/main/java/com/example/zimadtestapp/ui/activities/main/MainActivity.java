package com.example.zimadtestapp.ui.activities.main;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.zimadtestapp.R;
import com.example.zimadtestapp.ui.TabVisibilityListener;
import com.example.zimadtestapp.ui.activities.BaseActivity;
import com.example.zimadtestapp.ui.fragments.BaseFragment;
import com.example.zimadtestapp.ui.fragments.tab.TabFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<MainActivityViewModel> implements TabVisibilityListener {

    private static final String TAB_UNDEX_KEY = "TAB_UNDEX_KEY";

    @Override
    protected MainActivityViewModel provideViewModel(ViewModelProvider.Factory viewModelFactory) {
        return injectViewModel(MainActivityViewModel.class);
    }

    @Override
    protected int layout() { return R.layout.activity_main; }

    TabLayout tabLayout;
    int selectedTabIndex = 0;
    BaseFragment firstTabFragment = null;
    BaseFragment secondTabFragment = null;

    @Override
    protected void initialization(Bundle savedInstanceState) {
        tabLayout = findViewById(R.id.tabs_layout);

        if (savedInstanceState == null) {
            initTabFragments();
        }
        else {
            selectedTabIndex = savedInstanceState.getInt(TAB_UNDEX_KEY, 0);
        }
        initTabs();
        listenUpdates();

        BaseFragment fragment1 = getFragmentByTag(TabFragment.TabType.FIRST.name());
        if (fragment1 == null) selectTabFragment(selectedTabIndex);
    }

    private void initTabFragments(){
        firstTabFragment = TabFragment.getInstance(TabFragment.TabType.FIRST);
        secondTabFragment = TabFragment.getInstance(TabFragment.TabType.SECOND);
    }

    private void initTabs(){
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));

        selectTab(selectedTabIndex);
//        selectTab(tabLayout.getSelectedTabPosition());
    }

    private void selectTab(int position){
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        if (tab != null) tab.select();
    }

    private void listenUpdates(){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectTabFragment(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }
            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });
    }

    private void selectTabFragment(int tabIndex){
        BaseFragment fragment1 = getFragmentByTag(TabFragment.TabType.FIRST.name());
        if (fragment1 == null)
            fragment1 = TabFragment.getInstance(TabFragment.TabType.FIRST);

        BaseFragment fragment2 = getFragmentByTag(TabFragment.TabType.SECOND.name());
        if (fragment2 == null)
            fragment2 = TabFragment.getInstance(TabFragment.TabType.SECOND);

        switch(tabIndex){
            case 0:
                attachFragment(fragment1, fragment2);
                break;
            case 1:
                attachFragment(fragment2, fragment1);
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        selectedTabIndex = tabLayout.getSelectedTabPosition();
        outState.putInt(TAB_UNDEX_KEY, selectedTabIndex);

        FragmentManager manager = getSupportFragmentManager();
        for (Fragment fragment: manager.getFragments()){
            if (fragment instanceof TabFragment){
                Parcelable state = ((TabFragment)fragment).getListState();
                outState.putParcelable(
                        ((TabFragment)fragment).getFragmentTag(),
                        state);
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        FragmentManager manager = getSupportFragmentManager();
        for (Fragment fragment: manager.getFragments()){
            if (fragment instanceof TabFragment){
                ((TabFragment)fragment).setListState(
                        savedInstanceState.getParcelable(((TabFragment)fragment).getFragmentTag())
                );
            }
        }
    }

    @Override
    public void onTabVisibilityChange(boolean isVisible) {
        tabLayout.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }
}

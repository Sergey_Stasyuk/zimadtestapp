package com.example.zimadtestapp.ui.fragments.tab_detail;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import com.example.zimadtestapp.R;
import com.example.zimadtestapp.data.response.DataItem;
import com.example.zimadtestapp.ui.fragments.BaseFragment;
import com.example.zimadtestapp.utils.AppUtils;
import com.google.gson.Gson;

public class TabDetailFragment extends BaseFragment<TabDetailFragmentViewModel> {

    @Override
    public String getFragmentTag() {
        return TabDetailFragment.class.getName();
    }

    private static final String ITEM_DATA_KEY = "ITEM_DATA_KEY";
    public static TabDetailFragment getInstance(DataItem dataItem) {
        TabDetailFragment fragment = new TabDetailFragment();
        Bundle data = new Bundle();
        String stringData = new Gson().toJson(dataItem);
        data.putString(ITEM_DATA_KEY, stringData);
        fragment.setArguments(data);
        return fragment;
    }

    private static @NonNull DataItem getData(Bundle args){
        if (args != null){
            String stringData = args.getString(ITEM_DATA_KEY);
            if (stringData != null) {
                DataItem data = new Gson().fromJson(stringData, DataItem.class);
                if (data != null) return data;
            }
        }
        return new DataItem();
    }

    @Override
    protected TabDetailFragmentViewModel provideViewModel(ViewModelProvider.Factory viewModelFactory) {
        return injectViewModel(TabDetailFragmentViewModel.class);
    }

    private ImageView image;
    private View progress;
    private TextView title;
    private TextView subTitle;

    @Override
    protected int layout() { return R.layout.fragment_tab_detail; }

    @Override
    protected void initialization(View view, boolean isFirstInit) {
        tabVisibilityListener.onTabVisibilityChange(false);
        if (isFirstInit){
            image = view.findViewById(R.id.image_view);
            progress = view.findViewById(R.id.loading_spinner);
            title = view.findViewById(R.id.index_text_view);
            subTitle = view.findViewById(R.id.title_text_view);

            initViews();
        }
    }

    private void initViews(){
        DataItem dataItem = getData(getArguments());
        title.setText(String.valueOf(dataItem.getIndex()));
        subTitle.setText(dataItem.getTitle());
        AppUtils.setImage(image, progress, dataItem.getUrl());
    }
}

package com.example.zimadtestapp.ui.fragments.tab_detail;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.zimadtestapp.data.repository.ResponseCallbackInterface;
import com.example.zimadtestapp.data.response.TabResponse;
import com.example.zimadtestapp.mvvm.BaseViewModel;

public class TabDetailFragmentViewModel extends BaseViewModel {

    public TabDetailFragmentViewModel(@NonNull Application application) {
        super(application);
    }

}

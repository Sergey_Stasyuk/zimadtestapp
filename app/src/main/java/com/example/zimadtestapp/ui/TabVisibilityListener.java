package com.example.zimadtestapp.ui;

public interface TabVisibilityListener {

    void onTabVisibilityChange(boolean isVisible);

    TabVisibilityListener empty = new TabVisibilityListener(){
        @Override
        public void onTabVisibilityChange(boolean isVisible) { }
    };
}

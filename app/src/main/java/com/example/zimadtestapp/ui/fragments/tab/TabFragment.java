package com.example.zimadtestapp.ui.fragments.tab;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zimadtestapp.R;
import com.example.zimadtestapp.ui.adapter.TabListAdapter;
import com.example.zimadtestapp.ui.fragments.BaseFragment;
import com.example.zimadtestapp.ui.fragments.tab_detail.TabDetailFragment;

import java.util.ArrayList;

public class TabFragment extends BaseFragment<TabFragmentViewModel> {

    @Override
    public String getFragmentTag() {
        return getTabType(getArguments()).name();
    }

    public enum TabType {
        FIRST,
        SECOND
    }
    private static final String TAB_TYPE_KEY = "TAB_TYPE_KEY";
    public static TabFragment getInstance(TabType type) {
        TabFragment fragment = new TabFragment();
        Bundle data = new Bundle();
        data.putSerializable(TAB_TYPE_KEY, type);
        fragment.setArguments(data);
        return fragment;
    }
    private static @NonNull TabType getTabType(Bundle args){
        if (args != null){
            TabType type = (TabType)args.getSerializable(TAB_TYPE_KEY);
            if (type != null) return type;
        }
        return TabType.FIRST;
    }

    @Override
    protected TabFragmentViewModel provideViewModel(ViewModelProvider.Factory viewModelFactory) {
        return injectViewModel(TabFragmentViewModel.class);
    }

    private RecyclerView animalsList;
    private TabListAdapter adapter;
    private Parcelable listState;

    @Override
    protected int layout() { return R.layout.fragment_tab; }

    @Override
    protected void initialization(View view, boolean isFirstInit) {

        tabVisibilityListener.onTabVisibilityChange(true);

        if (isFirstInit){
            animalsList = view.findViewById(R.id.animals_list);

            initAdapter();
            listenUpdates();

            switch (getTabType(getArguments())){
                case FIRST: getFirstTabData(); break;
                case SECOND: getSecondTabData(); break;
            }

        }
    }

    public Parcelable getListState(){
        RecyclerView.LayoutManager manager = animalsList.getLayoutManager();
        return manager.onSaveInstanceState();
    }

    public void setListState(Parcelable state){
        listState = state;
    }

    private void restoreListState(Parcelable state){
        RecyclerView.LayoutManager manager = animalsList.getLayoutManager();
        if (manager != null) {
            manager.onRestoreInstanceState(state);
        }
    }

    private void initAdapter(){
        animalsList.setLayoutManager(new LinearLayoutManager(baseContext));
        adapter = new TabListAdapter(new ArrayList<>(), item -> {
            fragmentManager.addFragment(TabDetailFragment.getInstance(item));
            tabVisibilityListener.onTabVisibilityChange(false);
        });
        animalsList.setAdapter(adapter);
    }

    private void getFirstTabData(){
        showProgress(true);
        viewModel.getFirstTabData();
    }

    private void getSecondTabData(){
        showProgress(true);
        viewModel.getSecondTabData();
    }

    private void listenUpdates() {
        viewModel.firstTabLiveData.observe(this, tabResponse -> {
            showProgress(false);
            if (tabResponse != null){
                adapter.addAll(tabResponse.getData());
                restoreListState(listState);
            }
        });

        viewModel.secondTabLiveData.observe(this, tabResponse -> {
            showProgress(false);
            if (tabResponse != null){
                adapter.addAll(tabResponse.getData());
                restoreListState(listState);
            }
        });
    }
}

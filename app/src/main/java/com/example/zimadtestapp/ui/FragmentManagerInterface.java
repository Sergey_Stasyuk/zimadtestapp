package com.example.zimadtestapp.ui;

import androidx.annotation.Nullable;

import com.example.zimadtestapp.ui.fragments.BaseFragment;

public interface FragmentManagerInterface {

    void attachFragment(BaseFragment newFragment, BaseFragment currentFragment);
    void replaceFragment(BaseFragment newFragment);
    void addFragment(BaseFragment newFragment);
    @Nullable
    BaseFragment getFragmentByTag(String tag);

    FragmentManagerInterface empty = new FragmentManagerInterface() {
        @Override
        public void attachFragment(BaseFragment newFragment, BaseFragment currentFragment) { }
        @Override
        public void replaceFragment(BaseFragment newFragment) { }
        @Override
        public void addFragment(BaseFragment newFragment) { }
        @Nullable
        @Override
        public BaseFragment getFragmentByTag(String tag) { return null; }
    };
}

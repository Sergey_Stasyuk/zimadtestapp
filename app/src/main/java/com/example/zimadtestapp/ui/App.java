package com.example.zimadtestapp.ui;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.example.zimadtestapp.data.repository.ZiMadApi;
import com.example.zimadtestapp.mvvm.ViewModelFactory;

public class App extends Application {

    private ZiMadApi api;
    private ViewModelFactory viewModelFactory;

    public static App get(Activity activity) {
        return (App)activity.getApplication();
    }

    public static App get(Context context) {
        return (App)context;
    }

    public ZiMadApi getApi() {
        return api;
    }

    public ViewModelFactory getViewModelFactory() {
        return viewModelFactory;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        api = ZiMadApi.getInstance(this);
        viewModelFactory = new ViewModelFactory(this);
    }
}

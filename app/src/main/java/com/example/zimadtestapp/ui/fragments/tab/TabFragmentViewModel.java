package com.example.zimadtestapp.ui.fragments.tab;

import android.app.Application;
import android.os.Parcelable;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.zimadtestapp.data.repository.ResponseCallbackInterface;
import com.example.zimadtestapp.data.response.TabResponse;
import com.example.zimadtestapp.mvvm.BaseViewModel;

public class TabFragmentViewModel extends BaseViewModel {

    public TabFragmentViewModel(@NonNull Application application) {
        super(application);
    }

    MutableLiveData<TabResponse> firstTabLiveData = new MutableLiveData<>();
    MutableLiveData<TabResponse> secondTabLiveData = new MutableLiveData<>();

    public void getFirstTabData(){
        if (firstTabLiveData.getValue() == null) {
            api.getFirstTabData(new ResponseCallbackInterface<TabResponse>() {
                @Override
                public void onSuccess(TabResponse result) {
                    firstTabLiveData.postValue(result);
                }

                @Override
                public void onFailure(String errorMessage, int errorCode) {
                    Toast.makeText(getApplication(), errorMessage, Toast.LENGTH_LONG).show();
                    firstTabLiveData.postValue(null);
                }
            });
        }
    }

    public void getSecondTabData(){
        if (secondTabLiveData.getValue() == null) {
            api.getSecondTabData(new ResponseCallbackInterface<TabResponse>() {
                @Override
                public void onSuccess(TabResponse result) {
                    secondTabLiveData.postValue(result);
                }

                @Override
                public void onFailure(String errorMessage, int errorCode) {
                    Toast.makeText(getApplication(), errorMessage, Toast.LENGTH_LONG).show();
                    secondTabLiveData.postValue(null);
                }
            });
        }
    }

}

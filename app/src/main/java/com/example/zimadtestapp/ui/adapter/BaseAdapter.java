package com.example.zimadtestapp.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public abstract class BaseAdapter<T, H extends BaseAdapter.BaseViewHolder> extends RecyclerView.Adapter<H> {
    protected ArrayList<T> list;

    public BaseAdapter(ArrayList<T> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public H onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return createViewHolder(inflate(parent, viewType), viewType);
    }

    private View inflate(ViewGroup parent, @LayoutRes int res) {
        return LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
    }

    abstract H createViewHolder(View view, @LayoutRes int layoutId);

    @LayoutRes
    @Override
    public abstract int getItemViewType(int position);

    @Override
    public void onBindViewHolder(@NonNull H holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    abstract class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        abstract void bind(int pos);
    }

    public void addAll(ArrayList<T> list){
        this.list.addAll(list);
        notifyDataSetChanged();
    }

}

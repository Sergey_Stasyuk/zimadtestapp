package com.example.zimadtestapp.utils;

import android.view.View;
import android.widget.ImageView;

import com.example.zimadtestapp.R;
import com.squareup.picasso.Picasso;

public final class AppUtils {

    public static void setImage(ImageView image, View progress, String url){
        progress.setVisibility(View.VISIBLE);
        Picasso.get().load(url).into(image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        progress.setVisibility(View.GONE);
                    }
                }
        );
    }
}
